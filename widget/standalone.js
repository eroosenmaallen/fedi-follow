/**
 *  @file       html/standalone.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This module connects the HTML form in index.html to the fediFollow 
 *  library as a standalone widget.
 */

/*global fediFollow */

/** @typedef {object} QueryString
 *  @property {string} follow     URI of the account to follow; not necessarily an http URL
 *  @property {string} text       Text of a toot to post
 *  @property {string} style      Style overrides (NYI)
 *  @property {string} styleLight Style overrides (NYI)
 *  @property {string} styleDark  Style overrides (NYI)
 */
/**
 *  Read a search fragment and extract relevant elements
 *
 *  @param  {search} search   Search string from a URL like window.location
 *  @return {QueryString}     Extracted query string elements
 *  @alias module:fediFollow.loadQueryString
 */
function loadQueryString(search = window.location.search) {
  const sp = new URLSearchParams(search);
  const qs = {};

  // Are we following?
  if (sp.has('follow')) {
    qs.follow = sp.get('follow');
    qs.mode = 'follow';
  }
  else if (sp.has('url')) {
    // to support original version & documentation of this widget
    qs.follow = sp.get('url');
    qs.mode = 'follow';
  }
  
  if (sp.has('share')) {
    qs.share = sp.get('share');
    qs.mode = 'share';
  }

  if (sp.has('style')) {
    try {
      qs.style = JSON.parse(sp.get('style'));
    }
    catch (error) {
      console.error(`147: Unexpected error unpacking query string:`, error);
    }
  }
  if (sp.has('styleLight')) {
    try {
      qs.styleLight = JSON.parse(sp.get('styleLight'));
    }
    catch (error) {
      console.error(`147: Unexpected error unpacking query string:`, error);
    }
  }
  if (sp.has('styleDark')) {
    try {
      qs.styleDark = JSON.parse(sp.get('styleDark'));
    }
    catch (error) {
      console.error(`147: Unexpected error unpacking query string:`, error);
    }
  }

  return qs;
}

/**
 *  Initialize the standalone widget
 *
 *  This function will:
 *
 *  * Load saved settings from browser localStorage
 *  * examine the query string for url/follow/text, prefill the appropriate boxes,
 *    and en/disable boxes as appropriate, once implemented
 *  * attach event listeners to boxes to:
 *    * intercept form submit and save settings
 *    * catch changes to the instance, to format URLs correctly and update 
 *      the form action URL
 *
 *  @alias module:fediFollow.initStandalone
 */
// eslint-disable-next-line no-unused-vars
function initStandalone() {
  const {
    loadSavedSettings,
    saveSettings,
    detectPlatform,
    platformConfig,
    getTargetUrl,
    redirectTo,
   } = fediFollow;

  const form = document.getElementById('fedi');
  const saved = loadSavedSettings();

  const qs = loadQueryString();
  if (qs.follow)
    setWidgetMode('follow', qs.follow);
  if (qs.share)
    setWidgetMode('share', qs.share);
  if (qs.style) {
    try {
      const style = qs.style;
      console.debug(`106: qs.style=`, style);
      const allowedProps = [
        'outside-bg',
        'main-bg',
        'main-text',
        'main-border',
        'highlight-bg',
        'highlight-text',
        'highlight-border',
      ];
      for (const prop in style) {
        if (allowedProps.includes(prop))
          document.documentElement.style.setProperty(`--${prop}`, style[prop]);
      }
    }
    catch (error) {
      console.error('122: Failed to set styles:', error);
    }
  }

  if (!saved) {
    let el = document.getElementById('goButton');
    el.disabled = true;
    console.log('118: nothing to load');
    el = document.getElementById('instance');
    el.focus();
  }

  if (saved && Array.isArray(saved.instances)) {
    const list = document.getElementById('instances');
    saved.instances.forEach(inst => {
      const opt = document.createElement('option');
      opt.value = inst;
      list.appendChild(opt);
    });
    window.instances = saved.instances;

    let el = document.querySelector('#save');
    el.checked = true;
  }

  // Automatically restore the last saved instance
  if (saved && saved.lastInstance) {
    let el = document.getElementById('instance');
    el.value = saved.lastInstance;
    setWidgetInstance(saved.lastInstance);
  }

  form.addEventListener('submit', ev => {
    const { instance, mode, platform } = window;

    try {
      form.action = getTargetUrl(instance, mode, platform);
      console.debug(`254: form target set to`, form.action);
    }
    catch (error) {
      form.action = '#';
      console.warn(`256: Failed to find landing page URL:`, error.message, error);
      ev.preventDefault();
    }

    // set input names to matchplatformConfig
    let el = document.getElementById('url');
    el.name = platformConfig[platform].followParam || 'follow';

    el = document.getElementById('status');
    el.name = platformConfig[platform].shareParam || 'share';

    try {
      const el = document.getElementById('save');
      if (el && el.checked) {
        const url = new URL(window.instance);
        saveSettings(url.origin, window.instances);
      }
    }
    catch (error) {
      console.error('161: unexpected error saving settings: ', error);
    }

    if (platform === 'unknown' || form.action === '#' || !platformConfig[platform] || !platformConfig[platform][mode]) {
      ev.preventDefault();

      const data = mode === 'follow' ? form.url.value : form.status.value;
      const thing = mode === 'follow'
        ? 'account to follow'
        : 'toot to share';

      if (navigator.clipboard) {
        if (window.confirm(`Your instance platform was not detected or does not support remote ${mode}.
  Please click OK to copy the ${thing} to your clipboard and open your home instance...`)) {
          // do this in a callback, so the Document has the focus back(not the
          // Confirm window)
          setTimeout(() => {
            navigator.clipboard.writeText(data);
            redirectTo(instance, false);
          }, 100);
        }
      }
      else {
        window.alert(`Your instance platform was not detected or does not support ${mode}, and your clipboard is not available.`);
      }

      return;
    }

    console.debug('166: proceed to submitting form...', ev);

    //ev.preventDefault();
  });

  let el = document.getElementById('instance');
  el.addEventListener('input', ev => {
    ev.target.classList.remove('checked', 'error', 'checking');
  });
  el.addEventListener('change', ev => {
    try {
      ev.target.classList.remove('checked', 'error', 'checking');
      // force https
      ev.target.value = ev.target.value.replace(/^(https?:\/\/)?/, 'https://');
      setWidgetInstance(ev.target.value);
    }
    catch (error) {
      console.error('157: unexpected error applying instance:', error.message);
      ev.target.classList.add('error');
      //const el = document.createElement('div');
      return;
    }
  });

  el = document.getElementById('url');
  el.addEventListener('change', ev => {
    setWidgetMode(ev.target.value ? 'follow' : 'ready');
  });

  el = document.getElementById('status');
  el.addEventListener('change', ev => {
    setWidgetMode(ev.target.value ? 'share' : 'ready');
  });

  /**
   *  Set the widget's instance
   *
   *  @param {string} href URL of the instance
   */
  function setWidgetInstance(href) {
    const url = new URL(href);
    window.instance = url.origin;
    window.platform = 'unknown';

    console.log('180: set widget instance: ' + url.origin);
    
    const form = document.querySelector('form#fedi');
    form.action = '#';

    let el = document.getElementById('instance');
    el.classList.add('checked');
    el = document.getElementById('platformRow');
    el.hidden = false;
    el.textContent = 'checking instance type...';

    return detectPlatform(url).then(platform => {
      window.platform = platform;

      if (!platformConfig.hasOwnProperty(platform))
        platformConfig[platform] = {
          wag: true,                  // indicates this is a wild-assed guess
          share: '/share',
          follow: '/authorize_interaction',
        };

      console.debug(`305: set widget platform to "${platform}"`, platformConfig[platform]);

      const row = document.getElementById('platformRow');
      row.hidden = false;
      row.textContent = '';

      let platformEl = document.createElement('span');
      platformEl.className = 'platform';

      let icon = document.createElement('i');
      icon.className = `fa fa-${platform}`;
      icon.setAttribute('aria-hidden', true);
      platformEl.appendChild(icon);
      platformEl.appendChild(document.createTextNode(' ' + platform));

      row.appendChild(platformEl);

      if (!platformConfig[platform]) {
        row.appendChild(document.createElement('br'));
        const el = document.createElement('div');
        el.className = 'unsupported';
        el.textContent = `Unfortunately, ${platform} does not support external Share & Follow. We can use your clipboard, though!`;
        row.appendChild(el);
      }

      if (platform === 'unknown') {
        row.appendChild(document.createElement('br'));
        const el = document.createElement('div');
        el.className = 'unsupported';
        el.textContent = `Unfortunately, your platform could not be detected. We can use your clipboard, though!`;
        row.appendChild(el);
      }


      el = document.getElementById('goButton');
      el.disabled = false;
    });
  }

  /**
   *  Set widget mode; expose & activate relevant input(s) and set window.mode
   *
   *  @param {string} mode One of "follow", "share", or "ready"
   *  @param {string} data Value to prefill the "Follow"/"Share" box
   */
  function setWidgetMode(mode = 'ready', data) {
    console.debug(`327: set widget action/mode to "${mode}"`);
    window.mode = mode;

    if (mode === 'follow') {
      let el = document.querySelector('#followRow');
      el.hidden = false;

      el = form.querySelector('#url');
      el.disabled = false;
      if (data)
        el.value = data;


      el = document.querySelector('#shareRow');
      el.hidden = true;

      el = form.querySelector('#status');
      el.disabled = true;
      el.value = '';

      return mode;
    }
    if (mode === 'share') {
      let el = document.querySelector('#followRow');
      el.hidden = true;

      el = form.querySelector('#url');
      el.disabled = true;
      el.value = '';


      el = document.querySelector('#shareRow');
      el.hidden = false;

      el = form.querySelector('#status');
      el.disabled = false;
      if (data)
        el.value = data;

      return mode;
    }
    if (mode === 'ready') {
      let el = document.querySelector('#followRow');
      el.hidden = false;

      el = form.querySelector('#url');
      el.disabled = false;


      el = document.querySelector('#shareRow');
      el.hidden = false;

      el = form.querySelector('#status');
      el.disabled = false;

      return mode;
    }
    throw new RangeError(`330: "${mode}" is not a supported widget mode`);
  }
}
