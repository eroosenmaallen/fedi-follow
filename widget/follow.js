/**
 *  @file       js/follow.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *  
 *  This module provides an interface from the open web to the Fediverse, to
 *  more easily invite users to follow & share your content.
 *
 *  To use in vanilla browser:
 *  `<script src="/path/to/follow.js" />`
 *  `<body onload="fediFollow.initBrowser()">`
 *
 *  To use in nodejs or similar (not really tested, maybe not useful?):
 *  `const fediFollow = require('./path/to/follow.js');`
 *  `fediFollow.initNode();`
 *
 *  @module fediFollow
 */

/* exported fediFollow */
/* global module */

'use strict';

// In browser, fediFollow will be exported as a global object
// window.fediFollow; in Node or similar CommonJS environments, it
// will be exported as regular CommonJS exports
const fediFollow = (() => {

/** @typedef {object} PlatformConfig
 *  @property {string} share        Pathname to the "Share" landing page
 *  @property {string} shateParam   Name of the URL parameter to carry prefill text
 *  @property {string} follow       Pathname to the "Follow" landing page
 *  @property {string} followParam  Name of the URL parameter to carry the account to follow
 *  @property (Function()) detect   Function to detect this type of instance
 */
/** Struct of supported platforms, landing page 
 *  details, and detection methods
 *  @type {PlatformConfig[]}
 *  @alias module:fediFollow.platformConfig
 */
const platformConfig = {
  mastodon: {
    share: '/share',
    shareParam: 'text',
    follow: '/authorize_interaction',
    followParam: 'uri',
    detect: detectMastodon,
  },
  misskey: {
    share: '/share',
    shareParam: 'text',
    follow: '/authorize-follow',
    followParam: 'acct',
    detect: detectMisskey,
  },
  pleroma: {
    share: false, //'/share',
    shareParam: 'text',
    follow: '/ostatus_subscribe',
    followParam: 'acct',
    //detect: detectPleroma,
  },
  pixelfed: false,  // pixelfed doesn't appear to support external follow/share?
}


/**
 *  @typedef {object} SavedSettings
 *  @property {string?}   lastInstance  Most recently-used instance, to
 *    prefill "your instance" box
 *  @property {string[]?} instances     Array of other saved instances, to
 *    populate dropdown
 */
/**
 *  Load any settings previously saved to localStorage
 *
 *  @return {SavedSettings} Object with saved lastInstance and instances list
 *  @alias module:fediFollow.loadSavedSettings
 */
function loadSavedSettings() {
  console.debug('118: loadSavedSettings()');
  const empty = { instances: [] };

  if (typeof localStorage !== 'object' || !localStorage.fediFollow) {
    console.debug('118: nothing to load');
    return empty;
  }

  try { 
    const saved = JSON.parse(localStorage.getItem('fediFollow'));
    if (typeof saved !== 'object')
      return empty;

    console.debug('142: loaded options:', saved);
    return saved;
  }
  catch (error) {
    console.error('104: Unexpected error loading saved settings:', error.message, error.stack);
    return empty;
  }
}


/**
 *  Save the selected instance (and the dropdown items, if applicable)
 *
 *  @param  {string}    instance  Last-selected instance
 *  @param  {string[]}  instances All instances to save
 *  @return {SavedSettings}       Settings object as it was saved
 *  @alias module:fediFollow.saveSettings
 */
function saveSettings(instance, instances = []) {
  const saved = {
    instances: [],
  };

  if (Array.isArray(instances))
    instances.forEach(inst => saved.instances.push(inst));

  if (instance) {
    const inst = saved.lastInstance = new URL(instance).origin;
    if (!saved.instances.includes(inst))
      saved.instances.push(inst);
  }

  localStorage.setItem('fediFollow', JSON.stringify(saved));
  console.log('171: saved settings:', saved);

  return saved;
}



/**
 *  Get the target URL to direct a visitor to
 *
 *  @param  {string} instance Base instance URL (https://example.com/)
 *  @param  {string} action   Action to perform: follow, share
 *  @param  {string} platform Target platform: mastodon
 *  @return {string}          URL of the landing page, without any query string
 *  @alias module:fediFollow.getTargetUrl
 */
function getTargetUrl(instance, action = 'follow', platform = 'mastodon') {
  if (!platformConfig[platform])
    throw new RangeError(`Error: "${platform}" is not a supported platform`);
  if (!platformConfig[platform][action])
    throw new RangeError(`Error: "${action}" is not a supported action`);

  const u = new URL(instance);

  u.pathname = platformConfig[platform][action];

  return u.toString();
}

/**
 *  Detect the target instance's platform type (mastodon, misskey, etc)
 *
 *  @param  {string} origin Base origin for target instance (https://example.com)
 *  @return {string}        Detected platform type, or "unknown"
 *  @alias  module:fediFollow.detectPlatform
 */
async function detectPlatform(origin) {
  const probe = await probeNodeInfo(origin);
  console.debug('164: probe response:', probe);

  if (probe)
    return probe[0];

  for (const [name, platform] of Object.entries(platformConfig)) {
    if (platform.detect && await platform.detect(origin))
      return name;
  }

  return 'unknown';
}

/**
 *  Probe the target instance's nodeinfo endpoint; this may identify the
 *    server
 *
 *  @param  {string|URL} origin     Target instance's origin
 *  @return {Array<*>|boolean}      If found, element 0 will be the detected
 *    instance name, e[1] is the platformConfig; otherwise returns false
 */
async function probeNodeInfo(origin) {
  // first, poke the nodeinfo well-known endpoint to get the actual nodeinfo endpoint
  const u = new URL(origin);
  u.pathname = '/.well-known/nodeinfo';
  let req = await fetch(u, {
    redirect: 'follow',
  }).catch(error => {
    console.error('166: failed to probe .well-known/nodeinfo:', error);
    return {
      ok: false,
      message: error.message,
      error,
    };
  });

  if (!req.ok)
    return false;
  
  // Extract the actual nodeInfo link
  const nodeinfo = await req.json();
  const nodeinfo2Link = nodeinfo.links.find(l => {
    return l.rel === 'http://nodeinfo.diaspora.software/ns/schema/2.0';
  });
  console.debug('208: nodeinfo2Link:', nodeinfo2Link);

  if (nodeinfo2Link) {
    // fetch the NodeInfo2 endpoint
    req = await fetch(nodeinfo2Link.href).catch(error => {
      console.error('196: failed to probe ' + nodeinfo2Link.href, error.message, error);
      return {
        ok: false,
        message: error.message,
        error,
      };
    });
    if (req.ok) {
      // extract the software name
      const nodeinfo2 = await req.json();
      console.debug('223: nodeinfo2:', nodeinfo2);
      const software = nodeinfo2.software.name.toLowerCase();
      if (software)
        return [software, platformConfig[software]];
    }
  }

  return false;
}

/**
 *  Detect whether a target instance is Mastodon
 *
 *  @param  {string} origin Base origin for target instance (https://example.com)
 *  @return {number}        Confidence target is Mastodon, 0..1
 *  @internal
 */
async function detectMastodon(origin) {
  console.debug(`327: detecting Mastodon at ${origin}`);
  const u = new URL(origin);

  // Detect Mastodon:
  u.pathname = '/api/v1/instance';
  console.debug(`307: testing mastodon @ ${u}...`);
  const req = await fetch(u)
  .catch(error => {
    return {
      ok: false,
      message: error.message,
      cors: error.message === 'Failed to fetch',
      error,
    }
  });
  if (req.ok && req.headers.get('content-type').startsWith('application/')) {
    let inst = await req.json();

    if (inst.version) {
      console.debug(`313: Detected Mastodon API @ ${inst.version}`, inst);
      return 1.0;
    }
  }

  console.debug(`324: Not mastodon:`, req);

  return 0;
}


/**
 *  Detect whether a target instance is Misskey
 *
 *  @param  {string} origin Base origin for target instance (https://example.com)
 *  @return {number}        Confidence target is Misskey, 0..1
 *  @internal
 */
async function detectMisskey(origin) {
  console.debug(`283: detecting Misskey at ${origin}`);
  const u = new URL(origin);

  // Detect Misskey:
  u.pathname = '/api/meta';
  console.debug(`288: testing Misskey @ ${u}...`);
  const req = await fetch(u, { method: 'POST' })
  .catch(error => {
    return {
      ok: false,
      message: error.message,
      cors: error.message === 'Failed to fetch',
      error,
    }
  });
  if (req.ok && req.headers.get('content-type').startsWith('application/')) {
    let inst = await req.json();

    if (inst.version) {
      console.debug(`302: Detected Misskey API @ ${inst.version}`, inst);
      return 1.0;
    }
  }

  console.debug(`307: Not Misskey:`, req);

  return 0;
}


/**
 *  Ask the user for their instance (/account - NYI)
 *
 *  @param  {String} uxType   One of simple, standalone
 *  @return {string}          Instance's base origin
 *  @alias  module:fediFollow.getInstance
 */
async function getInstance(uxType = 'simple') {
  const saved = loadSavedSettings();

  let instance = fediFollow.lastInstance || saved.lastInstance || 'https://';

  switch (uxType) {
    default:
    case 'simple':
      instance = window.prompt(
        "Your instance:",
        instance
      );
      break;

    case 'standalone':
      instance = document.getElementById('instance').value;
  }

  if (!instance.startsWith('https://'))
    instance = instance.replace(/^(\w+:\/\/)?/, 'https://');
  instance = (new URL(instance)).origin;

  if (!saved.instances.includes(instance))
    saved.instances.push(instance);

  saveSettings(instance, saved.instances);

  return instance;
}


/**
 *  Redirect the user to their home instance to follow an account
 *
 *  @param  {string}  accountUri  Target account's URI
 *  @param  {Boolean} newWindow   Open a new window, if false use current window
 *  @return {Promise<boolean>}    Resolves to true if redirect was successful
 *  @alias  module:fediFollow.doFollow
 */
async function doFollow(accountUri, newWindow = true) {
  const instance = await getInstance();

  return detectPlatform(instance).then(platform => {
    const targetUrl = new URL(getTargetUrl(instance, 'follow', platform));
    const paramName = platformConfig[platform].followParam || 'uri';
    targetUrl.searchParams.set(paramName, accountUri);

    return redirectTo(targetUrl, newWindow);
  });
}


/**
 *  Redirect the user to their home instance to post a message
 *
 *  @param  {string}  text        Text to prefill for the Toot
 *  @param  {Boolean} newWindow   Open a new window, if false use current window
 *  @return {Promise<boolean>}    Resolves to true if redirect was successful
 *  @alias  module:fediFollow.doPostStatus
 */
async function doPostStatus(text, newWindow = true) {
  const instance = await getInstance();

  return detectPlatform(instance).then(platform => {
    const targetUrl = new URL(getTargetUrl(instance, 'share', platform));
    const paramName = platformConfig[platform].shareParam || 'text';
    targetUrl.searchParams.set(paramName, text);

    return redirectTo(targetUrl, newWindow);
  });
}

/**
 *  Direct the visitor to their home instance
 *
 *  @param  {string|URL}  targetUrl   Destination
 *  @param  {Boolean}     newWindow   false to redirect the current window
 *  @return {DOMWindow|string}        Handle to the new window, or url being
 *                                           redirected to
 *  @alias module:fediFollow.redirectTo
 */
function redirectTo(targetUrl, newWindow = true) {
  if (newWindow)
    return window.open(targetUrl, 'fediFollow', "innerWidth=800, innerHeight=600");
  else
    return window.location = targetUrl;
}

// The returned object will be exported in browser as window.fediFollow
return {
  // User-facing API
  doFollow,
  doPostStatus,
  redirectTo,
  getInstance,

  // More utility-type stuff
  detectPlatform,
  platformConfig,
  loadSavedSettings,
  saveSettings,
  getTargetUrl,
};

})();


// If we're in a CommonJS envronment, bang the fediFollows exports onto
// module.exports (but don't replace module.exports, because that can break
// CommonJS)
if (typeof module === 'object' && typeof module.exports === 'object') {
  Object.assign(module.exports, fediFollow);
}
