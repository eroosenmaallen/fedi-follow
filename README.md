# Fediverse Follow Widget

This widget provides a shortcut to invite people to follow an account or share
a toot on their Fediverse instance, using their instance's external
interaction URLs.


## What it does

The widget asks for the user's home instance, then probes the given URL to see
if it is a platform type we support (currently, only Mastodon).

If the instance is supported, the widget will construct the landing page URL
and redirect the user there to post or follow on their home turf.

If the widget does not detect a supported platform, then the account/toot will
be copied to the visitor's clipboard and they'll be sent on their way
(with their confirmation).

If the user chooses to remember their home instance, it is saved 
_only in their browser's local storage_; in particular, nothing is
ever sent back to me (the developer/host).


## How to use it

Simply link your visitor to the widget!


### To follow an account: `?follow=`

Send a `follow=` parameter to invite your visitor to follow your account:

https://eroosenmaallen.gitlab.io/fedi-follow/?follow=http://roosenmaallen.com/author/admin


### To post a toot or share a link: `?share=`

https://eroosenmaallen.gitlab.io/fedi-follow/?share=What%20a%20lovely%20day%20for%20a%20read%0A%0ahttps://roosenmaallen.com/


### To set colours: `?style=`

This is a work-in-progress, but for now you can send a JSON object containing
your colour overrides:

```
{
  "outside-bg",       // the area surrounding the widget
  "main-bg",          // background colour for the main form area
  "main-text",        // main text colour
  "main-border",      // Border around the outside of the widget
  "highlight-bg",     // background colour for the header and footer
  "highlight-text",   // header/footer text colour
  "highlight-border", // border between header/footer and main form
}
```

https://eroosenmaallen.gitlab.io/fedi-follow/?style=%7B%22outside-bg%22:%22pink%22%7D


The `style` param can be combined with the other two, but only one of `follow`
and `share` at a time make sense.


## Roadmap: Planned features

* Better styling support
* More instance types


## Contribute and support

The code is available at [GitLab](https://gitlab.com/eroosenmaallen/fedi-follow/);
feedback, contributions, pull requests, and bug reports are all welcome.

This widget is developed by [@silvermoon82@wandering.shop](https://wandering.shop/@silvermoon82),
whom you can tip at [Ko-Fi](https://ko-fi.com/silvermoon82).
